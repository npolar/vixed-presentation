# Vixed presentation

## What is Vixed?

A system for automatic delivery of data products to users in the field.
It is tailored to deliver products via thin and intermittent internet
connections.

System components:
- scheduler: advanced cron-like Go service
- processors: one time jobs with defined input and output interface written in
  python (or any other language)
- email interface: requests are submitted to and read from the service email
  account
- API interface (HTTP): requests can be submitted directly over HTTP
- QGIS plugins: for generating (and submitting) requests

### What data can Vixed provide?

- Iskart
- Værmelding (for punkt)
- Klorofyll-a-kart
- Nyheter (NRK)

## Why was Vixed created?

## Why should MET take over Vixed?

- Operational service 24/7
- MET has access to NRT satellite data (NP does not)
- Expertise in producing ice maps
- The crew has requested Arctic weather forecasting (halo.met.no)

## Quick links

- [Vixed-docs](npolar.gitlab.io/vixed-docs)
- [Vixed](gitlab.com/npolar/gis-and-eo/vixed)

## C4-diagrams

PlantUML diagrams can be found in C4_diagrams. They can be rendered as png files
by typing:

`java -jar plantuml.jar <PLANTUML FILE>.puml` (`plantuml.jar` as well as java
is needed)
